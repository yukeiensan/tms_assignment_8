# Installing requirements
`None`

# Usage
```
usage: value_calculator.exe [-h] [-f]

Mortgage amortization schedule generator

optional arguments:
  -h, --help    show this help message and exit
  -f, --future
```
