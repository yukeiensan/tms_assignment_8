#!/usr/bin/env python3

import argparse
import math
import csv


# Project functions
def parsing_arguments():
    parser = argparse.ArgumentParser(description="Mortgage amortization schedule generator")
    parser.add_argument("-f", "--future", action="store_true")
    args = parser.parse_args()

    return args


def round_half_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n * multiplier + 0.5) / multiplier


def calculate_present(value, rate, years):
    return value / pow((1+rate), years)


def calculate_future(value, rate, years):
    return value * pow((1+rate), years)


def get_value(text):
    print(">> Enter the " + text)
    cmd = ""

    while not isinstance(cmd, float):
        try:
            cmd = float(input(""))
        except ValueError:
            print("Please enter a valid number")

    return cmd


def main():
    data = {}
    args = parsing_arguments()

    if args.future:
        value_text = "present value"
    else:
        value_text = "future value"

    data["value"] = get_value(value_text)
    data["rate"] = get_value("rate")
    data["years"] = get_value("number of years")
    if args.future:
        result = calculate_future(data["value"], data["rate"], data["years"])
    else:
        result = calculate_present(data["value"], data["rate"], data["years"])
    print("Result: " + str(round(result, 2)))


if __name__ == "__main__":
    main()